const bouton = document.getElementById('jouer').addEventListener('click', validerEtCreer)

function validerEtCreer() {
   const NombreDePaires = document.getElementById('NP').value
   if (isNaN(NombreDePaires) || NombreDePaires < 2 || NombreDePaires > 10) {
      alert("données non valide")
   }
   else {
      document.getElementById('FormulaireCreation').style.display = "none"
      CreerJeux()
      document.getElementById('Timer').style.visibility = "visible"
   }
}



//jeux//

let CardsATournees

function CreerJeux() {
   startTimer()
   ObterCartesMelangees()
   genererCards()
}


function genererCards() {
   const parametres = obtenirParametres()
   const Jeux = document.getElementById('Jeux')
   const CartesMelangees = ObterCartesMelangees(parametres.nombre2)
   for (let i = 0; i < CartesMelangees.length; i++) {
      const Cards = document.createElement('input')
      Cards.class = "carte"
      Cards.style.width = "5em"
      Cards.style.height = "5em"
      Cards.value = "?"
      Cards.type = "button"
      Cards.onclick = "ClicCarte()"
      Cards.setAttribute("data-hide", CartesMelangees[i])
      Jeux.appendChild(Cards)
   }
   Cards.addEventListener("click", ClicCarte());
}

//timer//

function startTimer() {
   const temps = 5 * 60
   let s = temps % 60
   const m = Math.floor(temps / 60)
   if (s < 10) { s = "0" + s };
   document.getElementById('Timer').textContent = m + ":" + s;
   document.getElementById('Timer').setAttribute('data-seconds', temps)
   const intervale = setInterval(Less1Sec, 1000)
   return intervale
}
function Less1Sec() {
   var temps = document.getElementById('Timer').getAttribute('data-seconds')
   temps = parseInt(temps)
   temps = temps - 1;
   let s = temps % 60
   const m = Math.floor(temps / 60)
   if (s < 10 && s >= 0) { s = "0" + s };
   if (s < 0) { s = "59" };
   document.getElementById('Timer').textContent = m + ":" + s;
   document.getElementById('Timer').setAttribute('data-seconds', temps)
   if (temps === 0) {
      clearInterval(intervale)
   }
}

//random //
function ObterCartesMelangees(NombreDePaires) {

   const nbTours = NombreDePaires;
   const TableauCartes = [];
   for (let i = 0; i < nbTours; i++) {
      TableauCartes.push(i);
      TableauCartes.push(i);
   }

   const tableauCartesMelangees = []
   while (TableauCartes.length > 0) {
      const index = Math.floor(Math.random() * TableauCartes.length);
      tableauCartesMelangees.push(TableauCartes[index]);
      TableauCartes.splice(index, 1)
      console.log(TableauCartes);
      console.log(tableauCartesMelangees);
   }
   return tableauCartesMelangees
}
function obtenirParametres() {
   const champNombreCards = document.getElementById('NP')
   const NombreDePaires = document.getElementById('NP').value
   const nbTours = NombreDePaires;
   let nombreCards = champNombreCards.value
   nombreCards = parseInt(nombreCards)
   return { nombre1: nombreCards, nombre2: nbTours, }
}



//animation//
var active = 0;
var pairesTrouvees = 0;

var Cards = document.getElementsByClassName("carte");

function ClicCarte() {
   var activeCards = [];
   Cards[i].value = Cards[i].getAttribute("data-hide")
   if (active < 2) {
      cards[i].value = "data-hide";
      Cards[i].style.color = "red";
      active += 1;
      console.log(active);
   }
   if (active == 2) {
      var j = 0;
      for (let i = 0; i < activeCards.length; i += 1) {
         if (Cards[i].style.color === "red") {
            activeCards[j] = Cards[i];
            console.log(j + 'j');
            j += 1;
            if (j > 1) {
               j = 0;
            }
         }
      }
      if (activeCards[0].innerHTML == activeCards[1].innerHTML) {
         active = 0;
         pairesTrouvees += 1;
         if (pairesTrouvees == pairesATrouvees) {
            alert("Vous avez gagne");
         }
      } else if (activeCards[0].innerHTML !== activeCards[1].innerHTML) {
         setTimeout(RetournerCartesApres1Seconde(), 1000);
         active = 0;
      }
   }
}
function RetournerCartesApres1Seconde() {
   for (let i = 0; i < 2; i += 1) {
      Cards[i].attributes.removeNamedItem("value") = "?";
   }
}